﻿using UnityEngine;
using System.Collections;

public class AudioTriggers : MonoBehaviour {

	// Use this for initialization
	void Start () {
		AudioController.SetCategoryVolume( "Music", 1.0f );

	}

	// Update is called once per frame
	void Update () {
	
	}
	void OnTriggerEnter(Collider Player)
	{
		if (Player.gameObject.tag == "Player") {	
			if ((this.gameObject.name == "Area1") && (AudioController.IsPlaying ("Low") != true)) {
				AudioController.PlayMusic ("Low");
			} else if (this.gameObject.name == "Area2") {
				AudioController.PlayMusic ("Medium");

			} else if (this.gameObject.name == "Area3") {
				AudioController.PlayMusic ("High");
			}
		}
	}

}
