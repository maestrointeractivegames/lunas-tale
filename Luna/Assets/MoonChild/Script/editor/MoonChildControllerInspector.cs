﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace MaestroInteractive.MoonChild
{

	[CustomEditor (typeof(MoonChildController))]
	[CanEditMultipleObjects]

	/// <summary>
	/// Adds custom labels to the MoonChildController inspector
	/// </summary>

	public class MoonChildControllerInspector : Editor 
	{
		
		void onEnable()
		{
			// nothing
		}
		
		/// <summary>
		/// When inspecting a Corgi Controller, we add to the regular inspector some labels, useful for debugging
		/// </summary>
		public override void OnInspectorGUI()
		{
			MoonChildController controller = (MoonChildController)target;
			if (controller.State!=null)
			{
				EditorGUILayout.LabelField("Grounded",controller.State.IsGrounded.ToString());
				EditorGUILayout.LabelField("Falling",controller.State.IsFalling.ToString());
				EditorGUILayout.Space();
				EditorGUILayout.LabelField("Colliding Left",controller.State.IsCollidingLeft.ToString());
				EditorGUILayout.LabelField("Colliding Right",controller.State.IsCollidingRight.ToString());
				EditorGUILayout.LabelField("Colliding Above",controller.State.IsCollidingAbove.ToString());
				EditorGUILayout.LabelField("Colliding Below",controller.State.IsGrounded.ToString());
				EditorGUILayout.Space();
				EditorGUILayout.LabelField("Slope Angle",controller.State.SlopeAngle.ToString());
			}
			DrawDefaultInspector();		
		}
	}
}