﻿using UnityEngine;
using System.Collections;

namespace MaestroInteractive.MoonChild
{
	public class PauseButton : MonoBehaviour
	{
	    public virtual void PauseButtonAction()
	    {
	        GameManager.Instance.Pause();
	    }	
	}
}