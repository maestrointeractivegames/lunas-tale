using UnityEngine;
using System.Collections;

namespace MaestroInteractive.MoonChild
{	
	/// <summary>
	/// Add this class to an area (water for example) and it will pass its parameters to any character that gets into it.
	/// </summary>
	public class MoonChildControllerPhysicsVolume2D : MonoBehaviour 
	{
		public MoonChildControllerParameters ControllerParameters;
		public CharacterBehaviorParameters BehaviorParameters;	
	}
}