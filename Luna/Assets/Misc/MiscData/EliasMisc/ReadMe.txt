Thank you for buying this music package!

It contains a royalty free composition with three different intensity levels. 
Also included are stingers for Level Complete, Game Over and Extra (can be used for respawn etc.) that were specifically written to fit the composition.

The original complete theme was composed with ELIAS Composer Studio for use in the ELIAS Music Engine. 
You are using a baked version of it. The complete adaptive theme is also available here on the Asset Store and contains up to 30 minutes of unique music, along with key changes and 15 different stinger variations. 
Adaptive themes make the transitions better and the music will flow more intuitively.

ELIAS stands for Elastic Lightweight Integrated Audio System and is a new music engine designed to enhance the gaming experience with music that follows the story, just like it does in a movie. More information can be found in HowToUseTheEpro.txt.

For more information and content visit our website: http://eliassoftware.com

Feel free to mail questions directly to support@eliassoftware.com

Or call us at +46 8 5151 2500. You can reach us between 9 am - 5 pm CET (8 am - 4 pm GMT).

We would be grateful if you could rate this package in the Asset Store!