﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HeartContainer : MonoBehaviour {

    public GameObject MaskObject;
    private float MaskValue;
   


	// Use this for initialization
	void Start () {

	
	}
	
	// Update is called once per frame
	void Update () {

        //MaskValue = GameObject.Find("HeartFIllMask").GetComponent<Image>().fillAmount;
        MaskValue = MaskObject.GetComponent<Image>().fillAmount;

        if (MaskValue == 0) {
            this.GetComponent<Animator>().SetBool("Empty", true);
             }

        else {

            this.GetComponent<Animator>().SetBool("Empty", false);
                }

     

    }
}
