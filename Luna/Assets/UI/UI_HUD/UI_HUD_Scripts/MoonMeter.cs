﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MoonMeter : MonoBehaviour {

    public GameObject MeterFill;
    private float MaskValue;
    public float threshold = .2f;


    // Use this for initialization
    void Start()
    {


    }

    // Update is called once per frame
    void Update()
    {

        MaskValue = MeterFill.GetComponent<Image>().fillAmount;
   
        if (MaskValue ==0)
        { this.GetComponent<Animator>().SetBool("Empty", true); }

        else {

            this.GetComponent<Animator>().SetBool("Empty", false);

            if (MaskValue <= threshold)
                 {
                      this.GetComponent<Animator>().SetBool("Low", true);
                 }

             else {

                      this.GetComponent<Animator>().SetBool("Low", false);
                 }
        }


    }
}
